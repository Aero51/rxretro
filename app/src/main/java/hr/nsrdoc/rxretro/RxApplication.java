package hr.nsrdoc.rxretro;

import android.app.Application;

/**
 * Created by Lenovo on 25.11.2016..
 */

public class RxApplication extends Application {


    private NetworkService networkService;
    @Override
    public void onCreate() {
        super.onCreate();

        networkService = new NetworkService();

    }

    public NetworkService getNetworkService(){
        return networkService;
    }

}
