package hr.nsrdoc.rxretro.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import hr.nsrdoc.rxretro.R;

import java.util.List;


public class MainRecyclerViewAdapter extends RecyclerView.Adapter<MainRecyclerViewAdapter.ViewHolder> {

    private final List<Album> mValues;
    private final MainActivity.OnListInteractionListener mListener;

    public MainRecyclerViewAdapter(List<Album> items, MainActivity.OnListInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.main_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.userId.setText(mValues.get(position).getUserId());
        holder.id.setText(mValues.get(position).getId());
        holder.title.setText(mValues.get(position).getTitle());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView userId;
        public final TextView id;
        public final TextView title;
        public Album mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            userId = (TextView) view.findViewById(R.id.textView_userId);
            id = (TextView) view.findViewById(R.id.textView_id);
            title = (TextView) view.findViewById(R.id.textView_title);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + title.getText() + "'";
        }
    }
}
