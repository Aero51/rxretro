package hr.nsrdoc.rxretro.main;

import android.util.Log;


import java.util.List;

import hr.nsrdoc.rxretro.NetworkService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Observer;
import rx.Subscription;

/**
 * Created by Lenovo on 25.11.2016..
 */

public class MainPresenter implements IMainPresenter {

    private IMainView mView;
    private NetworkService mainService;
    private Subscription subscription;


    public MainPresenter(IMainView view, NetworkService service) {
        mView = view;
        mainService = service;


    }

    @Override
    public void loadRxList() {
        mView.showProgress();


        Observable<List<Album>> albumsResponseObservable = (Observable<List<Album>>)
                mainService.getPreparedObservable(mainService.getAPI().getAlbumsObservable(), Album.class, true, true);
        subscription = albumsResponseObservable.subscribe(new Observer<List<Album>>() {


            @Override
            public void onCompleted() {

                Log.d("onCompleted", "onCompleted");

            }

            @Override
            public void onError(Throwable e) {
                Log.d("Error", ": " + e.getLocalizedMessage());
            }

            @Override
            public void onNext(List<Album> models) {
                Log.d("OnNext", ": " + models.size());
                mView.displayList(models);
            }
        });
    }


    public void loadRetroList() {


        Call<List<Album>> call = mainService.getAPI().getAlbums();
        call.enqueue(new Callback<List<Album>>() {
            @Override
            public void onResponse(Call<List<Album>> call, Response<List<Album>> response) {
                Log.d("LastResponse", ": " + response.body().size());
                mView.displayList(response.body());
            }

            @Override
            public void onFailure(Call<List<Album>> call, Throwable t) {

            }
        });


    }

    @Override
    public void rxUnSubscribe() {
        if (subscription != null && !subscription.isUnsubscribed())
            subscription.unsubscribe();
    }


}
