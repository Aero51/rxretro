package hr.nsrdoc.rxretro.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import hr.nsrdoc.rxretro.NetworkService;
import hr.nsrdoc.rxretro.photos.PhotosActivity;
import hr.nsrdoc.rxretro.R;
import hr.nsrdoc.rxretro.RxApplication;

public class MainActivity extends AppCompatActivity implements IMainView {

    private OnListInteractionListener mListener;
    private MainRecyclerViewAdapter adapter;
    private IMainPresenter mPresenter;
    private ProgressBar progressBar;
    private NetworkService service;
    private List<Album> albumsList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));


        mListener = new OnListInteractionListener() {
            @Override
            public void onListFragmentInteraction(Album item) {
                Log.d("MainActivity", "item clicked: " + item.getId());

                Intent detailIntent = new Intent(MainActivity.this, PhotosActivity.class);
                detailIntent.putExtra("id", item.getId());

                startActivity(detailIntent);

            }
        };

        // adapter = new MainRecyclerViewAdapter(DummyContent.ITEMS, mListener);
        adapter = new MainRecyclerViewAdapter(albumsList, mListener);
        recyclerView.setAdapter(adapter);


        service = ((RxApplication) getApplication()).getNetworkService();
        mPresenter = new MainPresenter(this, service);


        mPresenter.loadRxList();


    }

    @Override
    protected void onPause() {
        super.onPause();
        mPresenter.rxUnSubscribe();

    }

    @Override
    public void displayList(List<Album> models) {
        albumsList.addAll(models);
        adapter.notifyDataSetChanged();
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void displayError() {

    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }


    public interface OnListInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Album albumItem);
    }

}
















