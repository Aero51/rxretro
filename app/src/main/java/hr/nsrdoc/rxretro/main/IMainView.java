package hr.nsrdoc.rxretro.main;

import java.util.List;

import hr.nsrdoc.rxretro.main.Album;

/**
 * Created by Lenovo on 25.11.2016..
 */

public interface IMainView {

    void displayList(List<Album> models);

    void displayError();

    void showProgress();


}
