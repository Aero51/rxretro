package hr.nsrdoc.rxretro.photos;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import hr.nsrdoc.rxretro.NetworkService;
import hr.nsrdoc.rxretro.R;
import hr.nsrdoc.rxretro.RxApplication;

/**
 * Created by Lenovo on 26.11.2016..
 */
public class PhotosActivity extends AppCompatActivity implements IPhotosView {

    private OnListInteractionListener mListener;
    private IPhotosPresenter mPresenter;
    private ProgressBar progressBar;
    private NetworkService service;
    private List<Photo> photosList = new ArrayList<>();
    private PhotosRecyclerViewAdapter adapter;
    private String albumsId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos);
        //getActionBar().setDisplayHomeAsUpEnabled(true);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        mListener = new OnListInteractionListener() {
            @Override
            public void onListFragmentInteraction(Photo item) {
                Log.d("PhotosActivity", "item clicked: " + item.getId());
            }
        };


        adapter = new PhotosRecyclerViewAdapter(photosList, mListener);
        recyclerView.setAdapter(adapter);




        service = ((RxApplication) getApplication()).getNetworkService();
        mPresenter = new PhotosPresenter(this, service);

        albumsId = getIntent().getStringExtra("id");

        mPresenter.loadRxList(albumsId);


    }


    @Override
    protected void onPause() {
        super.onPause();
        mPresenter.rxUnSubscribe();

    }

    @Override
    public void displayList(List<Photo> models) {
        progressBar.setVisibility(View.INVISIBLE);
        photosList.addAll(models);
       adapter.notifyDataSetChanged();
    }

    @Override
    public void displayError() {

    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }



    public interface OnListInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Photo photoItem);
    }
}
