package hr.nsrdoc.rxretro.photos;

/**
 * Created by Lenovo on 26.11.2016..
 */

public interface IPhotosPresenter {
    void loadRxList(String albumId);
    void rxUnSubscribe();

}
