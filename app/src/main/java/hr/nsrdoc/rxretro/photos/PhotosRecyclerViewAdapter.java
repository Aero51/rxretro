package hr.nsrdoc.rxretro.photos;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.util.List;

import hr.nsrdoc.rxretro.R;
import okhttp3.OkHttpClient;

/**
 * Created by Lenovo on 26.11.2016..
 */

public class PhotosRecyclerViewAdapter extends RecyclerView.Adapter<PhotosRecyclerViewAdapter.ViewHolder> {
    private final List<Photo> mValues;
    private final PhotosActivity.OnListInteractionListener mListener;

    public PhotosRecyclerViewAdapter(List<Photo> items, PhotosActivity.OnListInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public PhotosRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.photos_row, parent, false);
        return new PhotosRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PhotosRecyclerViewAdapter.ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        // Log.d("AlbumId:",mValues.get(position).getAlbumId());
        holder.albumId.setText(mValues.get(position).getAlbumId());
        holder.id.setText(mValues.get(position).getId());
        holder.title.setText(mValues.get(position).getTitle());

        Context context = holder.image.getContext();
        String url = mValues.get(position).getThumbnailUrl();

        //when ok http client is added to picasso loading of images with redirects is possible
        OkHttpClient client = new OkHttpClient();
        Picasso picasso = new Picasso.Builder(context).downloader(new OkHttp3Downloader(client)).build();

        picasso.load(url).into(holder.image);
        // Picasso.with(context).load(url).into(holder.image);


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView image;
        public final TextView albumId;
        public final TextView id;
        public final TextView title;
        public Photo mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            image = (ImageView) view.findViewById(R.id.picture);
            albumId = (TextView) view.findViewById(R.id.tvalbumId);
            id = (TextView) view.findViewById(R.id.tvid);
            title = (TextView) view.findViewById(R.id.tvtitle);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + title.getText() + "'";
        }
    }
}
