package hr.nsrdoc.rxretro.photos;

import java.util.List;

import hr.nsrdoc.rxretro.main.Album;

/**
 * Created by Lenovo on 26.11.2016..
 */

public interface IPhotosView {


    void displayList(List<Photo> models);

    void displayError();

    void showProgress();

}
