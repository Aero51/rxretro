package hr.nsrdoc.rxretro.photos;

import android.util.Log;

import java.util.List;

import hr.nsrdoc.rxretro.main.IMainPresenter;
import hr.nsrdoc.rxretro.NetworkService;

import rx.Observable;
import rx.Observer;
import rx.Subscription;

/**
 * Created by Lenovo on 26.11.2016..
 */

public class PhotosPresenter implements IPhotosPresenter {

    private IPhotosView mView;
    private NetworkService photosService;
    private Subscription subscription;

    public PhotosPresenter(IPhotosView mView, NetworkService service) {
        this.mView = mView;
        photosService = service;
    }


    @Override
    public void loadRxList(String albumId) {

        mView.showProgress();


        Observable<List<Photo>> albumsResponseObservable = (Observable<List<Photo>>)
                photosService.getPreparedObservable(photosService.getAPI().getPhotosObservable(Integer.parseInt(albumId)), Photo.class, true, true);
        subscription = albumsResponseObservable.subscribe(new Observer<List<Photo>>() {


            @Override
            public void onCompleted() {

                Log.d("onCompleted", "onCompleted");

            }

            @Override
            public void onError(Throwable e) {
                Log.d("Error", ": " + e.getLocalizedMessage());
            }

            @Override
            public void onNext(List<Photo> models) {
                Log.d("OnNext", ": " + models.size());
                mView.displayList(models);
            }
        });
    }


    @Override
    public void rxUnSubscribe() {

    }
}
